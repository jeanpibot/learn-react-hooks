import './css/App.css';
//components
import Header from './components/Header.jsx';
import Characters from './components/Characters';


function App() {
  return (
    <div className="App">
      <Header />
      <Characters />
    </div>
  );
}

export default App;
